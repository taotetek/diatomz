/*  =========================================================================
    diatomz - tiny tools for log streams

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef DIATOMZ_H_H_INCLUDED
#define DIATOMZ_H_H_INCLUDED

//  Include the project library file
#include "diatomz_library.h"

//  Add your own public definitions here, if you need them

#endif
