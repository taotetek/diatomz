/*  =========================================================================
    znormalizer - Wrapper for liblognorm

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef ZNORMALIZER_H_INCLUDED
#define ZNORMALIZER_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

//  @interface
//  Create a new znormalizer
DIATOMZ_EXPORT znormalizer_t *
    znormalizer_new (void);

DIATOMZ_EXPORT int
    znormalizer_load_rulebase (znormalizer_t *self, const char *path);

DIATOMZ_EXPORT void
    znormalizer_verbose (znormalizer_t *self);

DIATOMZ_EXPORT zframe_t *
    znormalizer_normalize (znormalizer_t *self, zframe_t *frame);

//  Destroy the znormalizer
DIATOMZ_EXPORT void
    znormalizer_destroy (znormalizer_t **self_p);

//  Self test of this class
DIATOMZ_EXPORT void
    znormalizer_test (bool verbose);

//  @end

#ifdef __cplusplus
}
#endif

#endif
