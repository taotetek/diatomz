/*  =========================================================================
    zpipeline - Pipeline actor

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

#ifndef ZPIPELINE_H_INCLUDED
#define ZPIPELINE_H_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _zpipeline_t zpipeline_t;

//  @interface
//  Create new zpipeline actor instance.
//  @TODO: Describe the purpose of this actor!
//
//      zactor_t *zpipeline = zactor_new (zpipeline, NULL);
//
//  Destroy zpipeline instance.
//
//      zactor_destroy (&zpipeline);
//
//  Enable verbose logging of commands and activity:
//
//      zstr_send (zpipeline, "VERBOSE");
//
//  Start zpipeline actor.
//
//      zstr_sendx (zpipeline, "START", NULL);
//
//  Stop zpipeline actor.
//
//      zstr_sendx (zpipeline, "STOP", NULL);
//
//  This is the zpipeline constructor as a zactor_fn;
DIATOMZ_EXPORT void
    zpipeline_actor (zsock_t *pipe, void *args);

//  Self test of this actor
DIATOMZ_EXPORT void
    zpipeline_test (bool verbose);
//  @end

#ifdef __cplusplus
}
#endif

#endif
