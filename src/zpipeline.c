/*  =========================================================================
    zpipeline - Pipeline actor

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                      
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    zpipeline - Pipeline actor
@discuss
@end
*/

#include "diatomz_classes.h"

//  Structure of our actor

struct _zpipeline_t {
    zsock_t *pipe;              //  Actor command pipe
    zpoller_t *poller;          //  Socket poller
    bool terminated;            //  Did caller ask us to quit?
    bool verbose;               //  Verbose logging enabled?
    //  TODO: Declare properties
};


//  --------------------------------------------------------------------------
//  Create a new zpipeline instance

static zpipeline_t *
zpipeline_new (zsock_t *pipe, void *args)
{
    zpipeline_t *self = (zpipeline_t *) zmalloc (sizeof (zpipeline_t));
    assert (self);

    self->pipe = pipe;
    self->terminated = false;
    self->poller = zpoller_new (self->pipe, NULL);

    //  TODO: Initialize properties

    return self;
}


//  --------------------------------------------------------------------------
//  Destroy the zpipeline instance

static void
zpipeline_destroy (zpipeline_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        zpipeline_t *self = *self_p;

        //  TODO: Free actor properties

        //  Free object itself
        zpoller_destroy (&self->poller);
        free (self);
        *self_p = NULL;
    }
}


//  Start this actor. Return a value greater or equal to zero if initialization
//  was successful. Otherwise -1.

static int
zpipeline_start (zpipeline_t *self)
{
    assert (self);

    //  TODO: Add startup actions

    return 0;
}


//  Stop this actor. Return a value greater or equal to zero if stopping 
//  was successful. Otherwise -1.

static int
zpipeline_stop (zpipeline_t *self)
{
    assert (self);

    //  TODO: Add shutdown actions

    return 0;
}


//  Here we handle incoming message from the node

static void
zpipeline_recv_api (zpipeline_t *self)
{
    //  Get the whole message of the pipe in one go
    zmsg_t *request = zmsg_recv (self->pipe);
    if (!request)
       return;        //  Interrupted

    char *command = zmsg_popstr (request);
    if (streq (command, "START"))
        zpipeline_start (self);
    else
    if (streq (command, "STOP"))
        zpipeline_stop (self);
    else
    if (streq (command, "VERBOSE"))
        self->verbose = true;
    else
    if (streq (command, "$TERM"))
        //  The $TERM command is send by zactor_destroy() method
        self->terminated = true;
    else {
        zsys_error ("invalid command '%s'", command);
        assert (false);
    }
    zstr_free (&command);
    zmsg_destroy (&request);
}


//  --------------------------------------------------------------------------
//  This is the actor which runs in its own thread.

void
zpipeline_actor (zsock_t *pipe, void *args)
{
    zpipeline_t * self = zpipeline_new (pipe, args);
    if (!self)
        return;          //  Interrupted

    //  Signal actor successfully initiated
    zsock_signal (self->pipe, 0);

    while (!self->terminated) {
        zsock_t *which = (zsock_t *) zpoller_wait (self->poller, 0);
        if (which == self->pipe)
            zpipeline_recv_api (self);
       //  Add other sockets when you need them.
    }
    zpipeline_destroy (&self);
}

//  --------------------------------------------------------------------------
//  Self test of this actor.

void
zpipeline_test (bool verbose)
{
    printf (" * zpipeline: ");
    //  @selftest
    //  Simple create/destroy test
    zactor_t *zpipeline = zactor_new (zpipeline_actor, NULL);

    zactor_destroy (&zpipeline);
    //  @end

    printf ("OK\n");
}
