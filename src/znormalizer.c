/*  =========================================================================
    znormalizer - class description

    Copyright (c) the Contributors as noted in the AUTHORS file.       
    This file is part of normalz.                                       
                                                                       
    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.           
    =========================================================================
*/

/*
@header
    normalz - 
@discuss
@end
*/

#include "diatomz_classes.h"

static void
debug_callback (void __attribute__((unused)) *cookie, const char *msg,
        size_t __attribute__((unused)) lenMsg)
{
    fprintf(stderr, "normalz: %s\n", msg);
}

//  Structure of our class

struct _znormalizer_t {
    ln_ctx lnctx;
    json_object *json;
};


//  --------------------------------------------------------------------------
//  Create a new znormalizer

znormalizer_t *
znormalizer_new (void)
{
    znormalizer_t *self = (znormalizer_t *) zmalloc (sizeof (znormalizer_t));
    assert (self);
    self->lnctx = ln_initCtx();
    assert(self->lnctx);
    return self;
}


//  --------------------------------------------------------------------------
//  Load a rulebase

int
znormalizer_load_rulebase (znormalizer_t *self, const char *path)
{
    return ln_loadSamples(self->lnctx, path);
}


//  --------------------------------------------------------------------------
//  Set verbose mode

void
znormalizer_verbose (znormalizer_t *self)
{
	ln_setDebugCB (self->lnctx, debug_callback, NULL);
	ln_enableDebug (self->lnctx, 1);
}


//  --------------------------------------------------------------------------
//  normalize a string

zframe_t *
znormalizer_normalize (znormalizer_t *self, zframe_t *frame)
{
    char *line = zframe_strdup (frame);
    zframe_destroy (&frame);
    ln_normalize (self->lnctx, line, strlen(line), &self->json);
    line = (char *)json_object_to_json_string (self->json);
    zframe_t *new_frame = zframe_from (line);
    zstr_free (&line);
    return new_frame;
}


//  --------------------------------------------------------------------------
//  Destroy the normalz

void
znormalizer_destroy (znormalizer_t **self_p)
{
    assert (self_p);
    if (*self_p) {
        znormalizer_t *self = *self_p;
        ln_exitCtx(self->lnctx);
        //  Free object itself
        free (self);
        *self_p = NULL;
    }
}

//  --------------------------------------------------------------------------
//  Self test of this class

void
znormalizer_test (bool verbose)
{
    printf (" * normalz: ");

    //  @selftest
    //  Simple create/destroy test
    znormalizer_t *self = znormalizer_new ();
    assert (self);
    
    znormalizer_verbose (self);

    int rc = znormalizer_load_rulebase (self, "./rulebases/sample.rulebase");
    assert (rc == 0);

    zframe_t *frame = zframe_from ("Quantity: 555");
    frame = znormalizer_normalize (self, frame);
    assert (rc == 0);

    char *want = "{ \"N\": \"555\", \"event.tags\": [ \"tag1\" ] }";
    char *got = zframe_strdup (frame);
    assert (!strcmp (want, got));
    printf ("%s\n", got);
   
    zframe_destroy (&frame); 
    znormalizer_destroy (&self);
    //  @end
    printf ("OK\n");
}
